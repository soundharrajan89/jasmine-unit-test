describe("createNote", function() {
    'use strict';
    var createNote = require('../src/createNote');
    var checkcreateNote;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkcreateNote = new createNote();
    });

    describe("createNote function", function () {

        //Spec for word match operation
        it("returns a note created from object 1 and leftover characters from object 2", function () {
            var sourceString = 'Some random words put together here. The output will contain what the user wants to convey. If the words do not match, the candidate will be killed kindly.';
            var textString = 'I love flowers on my cake. I also like white cream';
            var response = false;
            var stringMatchCheck = checkcreateNote.makeNote(sourceString, textString);
            console.log(stringMatchCheck);
            expect(stringMatchCheck.completeMatch).toEqual(response);
        });

        //Spec for word match operation
        it("returns a note created from object 1 and leftover characters from object 2", function () {
            var sourceString = 'Some random words put together here. The output will contain what the user wants to convey. If the words match, consider yourself lucky. If the words do not match, the candidate will be killed kindly.';
            var textString = 'I love flowers on my cake. I also like white cream';
            var response = true;
            var stringMatchCheck = checkcreateNote.makeNote(sourceString, textString);
            console.log(stringMatchCheck);
            expect(stringMatchCheck.completeMatch).toEqual(response);
        });

        it("returns a note created from object 1 and leftover characters from object 2", function () {
            var sourceString = 'Some random words put together here. The output will contain what the user wants to convey. If the words match, consider yourself lucky. If the words do not match, the candidate will be killed kindly.';
            var textString = 'I love flowers on my cake. I also like white cream';
            var response = 'SddputgtrhrThoutputontnwht the uer wants to one.  the words match, consider yoursef ucy. f the words do not match, the candidate will be illed kindly.';
            var stringMatchCheck = checkcreateNote.makeNote(sourceString, textString);
            console.log(stringMatchCheck);
            expect(stringMatchCheck.charactersLeftAtSource).toEqual(response);
        });

    });

});