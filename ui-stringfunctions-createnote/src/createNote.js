createNote = function() {};

createNote.prototype.makeNote = function(sourceString, noteString){
    /*
        WAP that accepts any number of arguments and adds all the numerical values together. 
        If any strings are passed, they are concatenated together.
        Boolean values will be counted together:. Eg True: 4, False: 3
        NULL values to be excluded.
        Also, output total number of arguments received.
        All results should be output to an object.
    */

    'use strict';

    var response = {
        finalNoteMessage:'',
        charactersNotMatched:'',
        charactersLeftAtSource:'',
    };

    var finalNoteMessage = '';

    //read through noteString characters.
    for ( var i = 0; i < noteString.length; i++ ) {
        if( noteString[i].charCode != 32 ){

            var sourceStringLength = sourceString.length; //update it for each character loop

            var charFound = false;

            //search character in sourceString and remove it from that position.
            for( var j = 0; j< sourceStringLength; j++){

                //check for character match
                if( noteString[i] === sourceString[j] ){

                    //add character to finalNoteMessage
                    response.finalNoteMessage = response.finalNoteMessage + noteString[i];
                    sourceString = sourceString.slice(0, j) + sourceString.slice(j+1);
                    charFound = true;
                    break;
                }
            }

            if( !charFound ){
                response.charactersNotMatched = response.charactersNotMatched + noteString[i];
            }

        }else{
            response.finalNoteMessage = response.finalNoteMessage + ' ';
        }
        //add left over characters of sourceString to response.
        response.charactersLeftAtSource = sourceString;
    }

    if( response.charactersNotMatched.length>0 ){
        response.completeMatch = false;
    }else{
        response.completeMatch = true;
    }

    //console.log( response );

    return response;
};
 
module.exports = createNote;