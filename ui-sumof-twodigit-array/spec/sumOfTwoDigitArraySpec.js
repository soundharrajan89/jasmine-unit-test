describe("sumOfArrayTwoDigits", function() {
    var sumOfArrayTwoDigits = require('../src/sumOfArrayTwoDigits');
    var calc;
 
    //This will be called before running each spec
    beforeEach(function() {
        calc = new sumOfArrayTwoDigits();
    });
 
    describe("Sum of two digit array number", function(){
         
        //Spec for two digits array sum operation
        it("Adding two digit number", function() {
            var passArray = [25,30,100];
            expect(calc.sum(passArray)).toEqual(55);
        });
         
    });
});