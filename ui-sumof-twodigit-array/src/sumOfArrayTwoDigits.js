SumOfArrayTwoDigits = function() {};


SumOfArrayTwoDigits.prototype.sum = function(array) {
    //return number1 + number2;
    var twoDigArray = [];
    for(i=0; i<=array.length-1; i++){
        var arrValue = array[i];
        var n = arrValue.toString();
        var numDig = arrValue.toString().split("");
        if(numDig.length == 2){
           twoDigArray.push(array[i]);
        }
    }
    var twoDigArraySum =  twoDigArray.reduce(function(a, b) { return a + b; }, 0);
    return twoDigArraySum;

}

module.exports = SumOfArrayTwoDigits;