timedifference = function() {};
 
timedifference.prototype.timedifferenceFunction = function(startTime, endTime){
    /*
        WAP to accept two timestamps of the same date from the user. 
        User will enter the date in the format (dd-mm-yyyy). 
        Check the time difference between the timestamps.
        Output as (X Hours, X Minutes)
    */

    var stTime = new Date(startTime);
    var enTime = new Date(endTime);
    var output = new Object();
    if( ( stTime.getDate()==enTime.getDate() ) && ( stTime.getMonth()==enTime.getMonth() ) && ( stTime.getFullYear()==enTime.getFullYear() ) ){
        //The dates match. Check time difference
        output.hours = enTime.getHours() - stTime.getHours();
        if (enTime.getMinutes() < stTime.getMinutes()) {
            output.mins = stTime.getMinutes() - enTime.getMinutes()
        } else {
            output.mins = enTime.getMinutes() - stTime.getMinutes();
        }
        var response = [];
        if (output.hours > 0) {
            response.push(output.hours + ' Hours');
        }
        if (output.mins > 0) {
            response.push(output.mins + ' Minutes');
        }
        //console.log(response);

        return response.join(', ');
    }else{
        console.log('Dates are different');
        return false;
    }
    
}
 
module.exports = timedifference;