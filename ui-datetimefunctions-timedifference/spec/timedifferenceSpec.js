describe("time difference", function() {
    var timedifference = require('../src/timedifference');
    var checktimedifference;
 
    //This will be called before running each spec
    beforeEach(function() {
        checktimedifference = new timedifference();
    });
 
    describe("checks two timezones", function(){
         
        //Spec for sum operation
        it("returns difference between the timestamp in hours & minutes", function() {
            var sourceObject = checktimedifference.timedifferenceFunction('02-22-2018 00:33:00','02-22-2018 17:00:00');
            console.log(sourceObject);
            expect(sourceObject).toEqual("17 Hours, 33 Minutes");
        });
         
    });
});