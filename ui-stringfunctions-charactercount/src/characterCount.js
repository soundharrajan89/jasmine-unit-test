characterCount = function() {};

characterCount.prototype.readNote = function(sourceString){
    /*
        WAP that accepts a string and returns the count of individual characters
    */

    'use strict';

    var responseArray = [];

    //check the first character, add it to a variable. find occurrences of that character across the complete string. remove the character from the string.
    //once the check completes, repeat with next unique character;

    var matcherString = sourceString.toLowerCase();
    var currentChar = '';

    while (matcherString.length > 0) {
        var charCount = 0;
        currentChar = matcherString[0];
        
        while (matcherString.indexOf(currentChar) > -1) {
            var index = matcherString.indexOf(currentChar);
            matcherString = matcherString.slice(0, index) + matcherString.slice(index+1);
            charCount++;
        }
        //console.log(currentChar + ' ' + matcherString.length);
        if( currentChar.charCodeAt(0)==32 ){
            continue;
        }
        responseArray.push([currentChar, charCount]);

    }
    var stringCount = '';
    for( var i in responseArray ){
        stringCount = stringCount + responseArray[i][0] + responseArray[i][1];
    }

    return stringCount;
};
 
module.exports = characterCount;