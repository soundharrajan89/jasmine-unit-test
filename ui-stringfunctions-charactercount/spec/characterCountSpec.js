describe("characterCount", function() {
    'use strict';
    var characterCount = require('../src/characterCount');
    var checkcharacterCount;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkcharacterCount = new characterCount();
    });

    describe("characterCount function", function () {

        //Spec for word match operation
        it("returns a string with the characters and their counts", function () {
            var sourceString = 'The Quick Brown Fox Jumps over the lazy dog';
            var countedArray = checkcharacterCount.readNote(sourceString);
            console.log(countedArray);
            var response = 't2h2e3q1u2i1c1k1b1r2o4w1n1f1x1j1m1p1s1v1l1a1z1y1d1g1';
            expect(countedArray).toEqual(response);
        });

    });

});