permutationMatch = function() {};

permutationMatch.prototype.match = function(sourceString, matchString){
    /*
        WAP that accepts a string and returns the count of individual characters
    */

    'use strict';

    var responseArray = [];

    //check the first character, add it to a variable. find occurrences of that character across the complete string. remove the character from the string.
    //once the check completes, repeat with next unique character;

    sourceString = sourceString.toLowerCase();
    var Char = '';
    var matches = true;
    var outputSequenceArray = [];
    var outputSequence = '';
    matchString = matchString.replace(/\s+/g,'').toLowerCase(); //removes all spaces & tabs and line breaks.

    for(var i=0; i<matchString.length; i++ ){
        var charCount = 0;
        Char = matchString[i];

        var checkIndex = sourceString.indexOf(Char);

        if ( checkIndex == -1) {
            matches = false;
            break;
        }

        outputSequenceArray.push( [checkIndex,Char] );

        sourceString = sourceString.slice(0, checkIndex) + sourceString.slice(checkIndex+1); //remove the character from source string so that it doesn't match again
    }

    outputSequenceArray.sort(sortFunction);

    function sortFunction(a,b){
        if (a[1] === b[1]) {
            return 0;
        }
        else {
            return (a[1] < b[1]) ? -1 : 1;
        }
    }

    for( var i in outputSequenceArray ){
        outputSequence = outputSequence + outputSequenceArray[i][1];
    }

    console.log(outputSequence);

    return matches;
};
 
module.exports = permutationMatch;