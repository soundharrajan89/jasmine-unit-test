describe("permutationMatch", function() {
    'use strict';
    var permutationMatch = require('../src/permutationMatch');
    var checkpermutationMatch;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkpermutationMatch = new permutationMatch();
    });

    describe("permutationMatch function", function () {

        //Spec for word match operation
        it("returns true if a larger string contains a permutation of the matcher string", function () {
            var sourceString = 'The Quick Brown Fox Jumps over the lazy dog';
            var matchString = "Sock";
            var countedArray = checkpermutationMatch.match(sourceString, matchString);
            console.log(matchString + ' ' + countedArray);
            expect(countedArray).toBeTruthy();
        });

        //Spec for word match operation
        it("returns true if a larger string contains a permutation of the matcher string", function () {
            var sourceString = 'The Quick Brown Fox Jumps over the lazy dog';
            var matchString = "Socks";
            var countedArray = checkpermutationMatch.match(sourceString, matchString);
            console.log(matchString + ' ' + countedArray);
            expect(countedArray).toBeFalsy();
        });

    });

});