fibonacci = function() {};

fibonacci.prototype.count = function(counter){
    /*
        WAP that accepts a counter and creates that many fibonnacci numbers. Returns the last number in the series. 
        Use recursion
    */

    'use strict';

    var iterate = function( n ){
        if (n===1) 
        {
            return [0, 1];
        } 
        else 
        {
            var s = iterate(n - 1);
            s.push(s[s.length - 1] + s[s.length - 2]);
            return s;
        }
    };

    var series = iterate(counter);
    var lastNumber = series[series.length-1];

    return lastNumber;
};
 
module.exports = fibonacci;