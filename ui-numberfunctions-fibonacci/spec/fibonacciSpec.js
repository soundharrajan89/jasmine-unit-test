describe("fibonacci", function() {
    'use strict';
    var fibonacci = require('../src/fibonacci');
    var checkfibonacci;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkfibonacci = new fibonacci();
    });

    describe("fibonacci function", function () {

        //Spec for word match operation
        it("returns the last fibonacci number on the list based on a counter", function () {
            var match = 13;
            var number = checkfibonacci.count(7);
            console.log(7 + 'th: ' + number);
            expect(number).toEqual(match);
        });

        //Spec for word match operation
        it("returns the last fibonacci number on the list based on a counter", function () {
            var match = 21;
            var number = checkfibonacci.count(8);
            console.log(8 + 'th: ' + number);
            expect(number).toEqual(match);
        });

    });

});