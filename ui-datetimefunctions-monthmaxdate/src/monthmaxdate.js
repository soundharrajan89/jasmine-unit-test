monthmaxdate = function() {};
 
monthmaxdate.prototype.monthmaxdateFunction = function(dateValue){
    /*
        WAP to accept a date from the user. 
        User will enter the date in the format (dd-mm-yyyy). 
        Check which month the date falls in, and find out the max number of days in that month. 
        Output as an object: {month: January, days:31}
    */

    var d = new Date(dateValue);
    var maxd= new Date(d.getFullYear(), d.getMonth()+1, 0);
    var monthlist = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    var output = {
        month:String,
        days:Number
    }
    output.month = monthlist[d.getMonth()];
    output.days = maxd.getDate();
    return output;
}
 
module.exports = monthmaxdate;