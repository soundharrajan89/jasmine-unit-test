describe("monthmaxdate", function() {
    var monthmaxdate = require('../src/monthmaxdate');
    var checkmonthmaxdate;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkmonthmaxdate = new monthmaxdate();
    });
 
    describe("monthmaxdate function", function(){
         
        //Spec for sum operation
        it("monthmaxdate(string reverse testing) testing", function() {
            var responseDateObject = {
                month: "December",
                days: 31
            }
            var sourceObject = checkmonthmaxdate.monthmaxdateFunction('12-12-2017');
            console.log(sourceObject);
            expect(sourceObject).toEqual(responseDateObject);
        });
         
    });
});