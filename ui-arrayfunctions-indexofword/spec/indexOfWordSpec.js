describe("indexOfWord", function() {
    var indexOfWord = require('../src/indexOfWord');
    var checkindexOfWord;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkindexOfWord = new indexOfWord();
    });
 
    describe("indexOfWord function", function(){
         
        //Spec for sum operation
        it("returns occurence of a word in an array", function() {
            var array = ["Janice","Tom","Krishna","Kavita","Jayanti","Pandu","Otaktu","Tchalla"];
            var word = 'Kavita';
            var response = 3;
            var sourceObject = checkindexOfWord.indexOfWordFunction(array, word);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });

        //Spec for sum operation
        it("returns occurence of a word in an array", function() {
            var array = ["Janice","Tom","Krishna","Kavita","Jayanti","Pandu","Otaktu","Tchalla"];
            var word = 'Trebol';
            var sourceObject = checkindexOfWord.indexOfWordFunction(array, word);
            console.log(sourceObject);
            expect(sourceObject).toBeFalsy();
        });

    });
});