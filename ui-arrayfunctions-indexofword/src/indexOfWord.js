indexOfWord = function() {};

indexOfWord.prototype.indexOfWordFunction = function(arrayOfWords,word){
    /*
        WAP that accepts an array of words from the user and checks for a particular word, returning the index.
        Do not use a FOR loop.
    */
   var index = arrayOfWords.indexOf(word);
    if( index == -1){
        return false;
    }
    return index;
}
 
module.exports = indexOfWord;