wordcount3 = function() {};

wordcount3.prototype.wordcount3Function = function(aVeryLongString){
    /*
        WAP that accepts a string from the user and counts the number of words in the string per sentence.
        Any special characters such as punctuation marks, etc should not be counted along with the words
    */
    var wordcount = 0;
    var words;
    var smallerwords = 0;
    var sentenceSplit = aVeryLongString.match( /[^\.!\?]+[\.!\?]+/g ); //splits text to sentences based on punctuation marks
    for( var s in sentenceSplit){
        var strippedSentence = sentenceSplit[s].replace(/[.,\/#!$%\^&\*;:'"{}=\-_`~()]/g,"");
        words = strippedSentence.split(" ");
        for( var w in words ){
            if(words[w]=='' || words[w].length<4){
                words.splice(w, 1);
                smallerwords ++;
            }
        }
        wordcount = wordcount + words.length;
    }
    console.log('Words having 3 or less letters: ' + smallerwords);
    return wordcount;
}
 
module.exports = wordcount3;