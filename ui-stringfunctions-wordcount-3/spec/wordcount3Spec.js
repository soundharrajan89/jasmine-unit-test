describe("wordcount3", function() {
    var wordcount3 = require('../src/wordcount3');
    var checkwordcount3;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkwordcount3 = new wordcount3();
    });
 
    describe("wordcount3 function", function(){
         
        //Spec for sum operation
        it("returns count of words having more than 3 letters in a text", function() {
            var string = "The Quick Brown Fox Jumps over the lazy dog. The Lazy dog screamed: \"Stop jumping over me!\" He then kicked the fox on his face."
            var response = 17;
            var sourceObject = checkwordcount3.wordcount3Function(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });
         
    });
});