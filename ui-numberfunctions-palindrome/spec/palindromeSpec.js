describe("palindrome", function() {
    var palindrome = require('../src/palindrome');
    var checkPalindrome;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkPalindrome = new palindrome();
    });
 
    describe("Palindrome function", function(){
         
        //Spec for sum operation
        it("Palindrome(string reverse testing) testing", function() {
            expect(checkPalindrome.palindromeFunction("malayalam")).toEqual("malayalam");
        });
         
    });
});