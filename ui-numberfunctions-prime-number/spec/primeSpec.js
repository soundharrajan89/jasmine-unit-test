describe("prime", function() {
    var prime = require('../src/prime');
    var checkPrime;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkPrime = new prime();
    });
 
    describe("Will checking prime number or not", function(){
         
        //Spec for sum operation
        it("Should checking prime number", function() {
            expect(checkPrime.primeCheck(37)).toEqual(true);
        });
		
		it("Should checking not prime number", function() {
            expect(checkPrime.primeCheck(35)).toEqual(false);
        });
         
    });
});