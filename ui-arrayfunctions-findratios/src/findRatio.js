findRatio = function() {};

findRatio.prototype.commonFn = function(callFn){

    function gcd2(a, b) {
        // Greatest common divisor of 2 integers
        if (!b) return b === 0 ? a : NaN;
        return gcd2(b, a % b);
    }
    function gcd(array) {
        // Greatest common divisor of a list of integers
        var n = 0;
        for (var i = 0; i < array.length; ++i)
            n = gcd2(array[i], n);
        return n;
    }
    function lcm2(a, b) {
        // Least common multiple of 2 integers
        return a * b / gcd2(a, b);
    }
    function lcm(array) {
        // Least common multiple of a list of integers
        var n = 1;
        for (var i = 0; i < array.length; ++i)
            n = lcm2(array[i], n);
        return n;
    }
    if( callFn=='gcd' ){
        return gcd;
    }else{
        return lcm;
    }
}

findRatio.prototype.reduceRatios = function( arrayObj ){
    /*
        WAP to convert all array values into relative ratios
        Eg: 1200, 300,500,100 becomes 12,3,5,1
    */

    

    var reducedArray = [];

    var base = this.commonFn('gcd')(arrayObj);

    console.log(`common factor for ${arrayObj} : ${base}`);

    for(var i in arrayObj){
        reducedArray.push( arrayObj[i]/base );
    }

    return reducedArray;

};

findRatio.prototype.getMultiplier = function( arrayObj ){

    /*
        WAP to find the multiplying factor for an array of numbers.
    */
    

    var reducedArray = [];

    var base = this.commonFn('lcm')(arrayObj);
    console.log(`common multiple for ${arrayObj} : ${base}`);

    for(var i in arrayObj){
        reducedArray.push( base/arrayObj[i] );
    }

    return reducedArray;
}

module.exports = findRatio;