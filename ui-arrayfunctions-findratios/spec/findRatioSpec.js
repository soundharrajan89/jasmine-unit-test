describe("Ratios", function() {
    'use strict';
    var findRatio = require('../src/findRatio');
    var checkfindRatio;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkfindRatio = new findRatio();
    });
 
    describe("findRatio function", function () {

        //Spec for sum operation
        it("reduces an array of numbers to it's lowest form", function () {
            var response = checkfindRatio.reduceRatios([12, 48, 24, 36, 6, 3, 9]);
            console.log(response);
            expect(response).toEqual([4, 16, 8, 12, 2, 1, 3]);
        });

    });
    describe("Multiplier function", function () {

        //Spec for sum operation
        it("returns an array of factors given an array to increase to it's max value", function () {
            var response = checkfindRatio.getMultiplier([90, 48, 24, 36, 6, 3, 9]);
            console.log(response);
            expect(response).toEqual([8, 15, 30, 20, 120, 240, 80]);
        });

    });
});