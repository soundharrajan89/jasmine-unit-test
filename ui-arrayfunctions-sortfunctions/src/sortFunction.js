sortFunction = function() {};



sortFunction.prototype.byNumber = function( arrayObject, direction ){
    /*
        WAP that accepts a 2 dimensional array (numbers, alphabets)
        Sorts the array by the number column.
        Direction of sort sent as a parameter
    */

    'use strict';

    if( direction === 'reverse' ) {

        arrayObject.sort(reverse);

    }else{

        arrayObject.sort(forward);

    }

    function reverse(a,b){
        if (a[0] === b[0]) {
            return 0;
        }
        else {
            return (a[0] > b[0]) ? -1 : 1;
        }
    }

    function forward(a,b){
        if (a[0] === b[0]) {
            return 0;
        }
        else {
            return (a[0] < b[0]) ? -1 : 1;
        }
    }

    //console.log( arrayObject );
    
    return arrayObject;
};

sortFunction.prototype.byCharacter = function( arrayObject, direction ){
    /*
        WAP that accepts a 2 dimensional array (numbers, alphabets)
        Sorts the array by the alphabet column.
        Direction of sort sent as a parameter
    */

    'use strict';

    if( direction === 'reverse' ) {

        arrayObject.sort(reverse);

    }else{

        arrayObject.sort(forward);

    }

    function reverse(a,b){
        if (a[1] === b[1]) {
            return 0;
        }
        else {
            return (a[1] > b[1]) ? -1 : 1;
        }
    }

    function forward(a,b){
        if (a[1] === b[1]) {
            return 0;
        }
        else {
            return (a[1] < b[1]) ? -1 : 1;
        }
    }

    //console.log( arrayObject );

    return arrayObject;
};
 
module.exports = sortFunction;