describe("sortFunction", function() {
    'use strict';
    var sortFunction = require('../src/sortFunction');
    var checksortFunction;
 
    //This will be called before running each spec
    beforeEach(function() {
        checksortFunction = new sortFunction();
    });
 
    describe("sortFunction function", function () {
         
        //Spec for sum operation
        it("returns sorted array by 1st column of a 2 dimensional array", function () {
            var array = [[4,"A"],[8,"Z"],[5,"Y"],[1,"T"],[2,"I"],[24,"Q"],[3,"M"],[9,"H"],[10,"P"],[12,"W"],[5,"N"],]
            var sourceObject = checksortFunction.byNumber(array, 'forward');
            //console.log(sourceObject);
            expect(sourceObject[0]).toEqual([1,"T"]);
        });

        //Spec for sum operation
        it("returns reverse sorted array by 1st column of a 2 dimensional array", function () {

            var array = [[4,"A"],[8,"Z"],[5,"Y"],[1,"T"],[2,"I"],[24,"Q"],[3,"M"],[9,"H"],[10,"P"],[12,"W"],[5,"N"],]
            var sourceObject = checksortFunction.byNumber(array, 'reverse');
            //console.log(sourceObject);
            expect(sourceObject[0]).toEqual([24,"Q"]);
        });

        //Spec for sum operation
        it("returns sorted array by 2nd column of a 2 dimensional array", function () {

            var array = [[4,"A"],[8,"Z"],[5,"Y"],[1,"T"],[2,"I"],[24,"Q"],[3,"M"],[9,"H"],[10,"P"],[12,"W"],[5,"N"],]
            var sourceObject = checksortFunction.byCharacter(array, 'forward');
            //console.log(sourceObject);
            expect(sourceObject[0]).toEqual([4,"A"]);
        });

        //Spec for sum operation
        it("returns reverse sorted array by 2nd column of a 2 dimensional array", function () {

            var array = [[4,"A"],[8,"Z"],[5,"Y"],[1,"T"],[2,"I"],[24,"Q"],[3,"M"],[9,"H"],[10,"P"],[12,"W"],[5,"N"],]
            var sourceObject = checksortFunction.byCharacter(array, 'reverse');
            //console.log(sourceObject);
            expect(sourceObject[0]).toEqual([8,"Z"]);
        });

    });
});