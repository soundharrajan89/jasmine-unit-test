describe("wordcount2", function() {
    var wordcount2 = require('../src/wordcount2');
    var checkwordcount2;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkwordcount2 = new wordcount2();
    });
 
    describe("wordcount2 function", function(){
         
        //Spec for sum operation
        it("returns count of words per sentence in an array", function() {
            var string = "The Quick Brown Fox Jumps over the lazy dog. The Lazy dog screamed: \"Stop jumping over me!\" He then kicked the fox on his face."
            var response = [9,16];
            var sourceObject = checkwordcount2.wordcount2Function(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });
         
    });
});