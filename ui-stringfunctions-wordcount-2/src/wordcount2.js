wordcount2 = function() {};

wordcount2.prototype.wordcount2Function = function(aVeryLongString){
    /*
        WAP that accepts a string from the user and counts the number of words in the string per sentence.
        Any special characters such as punctuation marks, etc should not be counted along with the words
    */
    var wordcount = [];
    var words;
    var sentenceSplit = aVeryLongString.split( /[\.!?]+[ \n]+/g ); //splits text to sentences based on punctuation marks
    for( var s in sentenceSplit){
        var strippedSentence = sentenceSplit[s].replace(/[.,\/#!$%\^&\*;:'"{}=\-_`~()]/g,"");
        strippedSentence = strippedSentence.replace(/\s+/g," ");
        words = strippedSentence.split(/\s+/g);
        for( var w in words ){
            if(words[w]==''){
                words.splice(w, 1);
            }
        }
        wordcount.push(words.length);
        //console.log(words);
    }
    return wordcount;
}
 
module.exports = wordcount2;