describe("timeinwords", function() {
    var timeinwords = require('../src/timeinwords');
    var checktimeinwords;
 
    //This will be called before running each spec
    beforeEach(function() {
        checktimeinwords = new timeinwords();
    });
 
    describe("timeinwords function", function(){
         
        //Spec for sum operation
        it("returns time in words", function() {
            var sourceObject = checktimeinwords.timeinwordsFunction('11-05-1988 17:33:00');
            console.log(sourceObject);
            expect(sourceObject).toEqual("Five Thirty-three PM");
        });
         
    });
});