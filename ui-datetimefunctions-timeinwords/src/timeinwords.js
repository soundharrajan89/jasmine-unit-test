monthandday = function() {};
 
monthandday.prototype.toTitleCase = function (str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

monthandday.prototype.timeinwordsFunction = function(userTime){
    /*
        WAP that accepts the date & time from the user and outputs the date in words
    */
    var dtime = new Date(userTime);
    var hours = dtime.getHours();
    var mins = dtime.getMinutes();
    var singles = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    var tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty'];
    var timestamp = new Object();
    if(hours>12){
        timestamp.period = "PM";
        hours = hours - 12;
    }else{
        timestamp.period = "AM";
    }

    timestamp.hours = singles[hours];
    if(mins<20){
        timestamp.minutes = singles[mins];
    }else{
        var m = mins.toString();
        timestamp.minutes = tens[m[0]] + '-' + singles[m[1]];
    }

    return this.toTitleCase(timestamp.hours) + ' ' + this.toTitleCase(timestamp.minutes) + ' ' + timestamp.period;
}
 
module.exports = monthandday;