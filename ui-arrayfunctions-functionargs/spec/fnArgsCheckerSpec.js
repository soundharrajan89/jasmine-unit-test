describe("fnArgsChecker", function() {
    'use strict';
    var fnArgsChecker = require('../src/fnArgsChecker');
    var checkfnArgsChecker;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkfnArgsChecker = new fnArgsChecker();
    });
 
    describe("fnArgsChecker function", function () {
         
        //Spec for sum operation
        it("returns array of arguments passed to a function", function () {
            var response = {
                numbers: 29,
                strings: '34234hhsdggdingdongfalse',
                boolean: {
                    true: 2,
                    false: 1
                }
            };
            var sourceObject = checkfnArgsChecker.fetchArguments( 1, 2, 3, "34234", "hhsdgg", true, "dingdong", "false", true, false, 23 );
            //console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });

    });
});