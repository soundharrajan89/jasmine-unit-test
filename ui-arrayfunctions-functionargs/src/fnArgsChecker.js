fnArgsChecker = function() {};

fnArgsChecker.prototype.fetchArguments = function(){
    /*
        WAP that accepts any number of arguments and adds all the numerical values together. 
        If any strings are passed, they are concatenated together.
        Boolean values will be counted together:. Eg True: 4, False: 3
        NULL values to be excluded.
        Also, output total number of arguments received.
        All results should be output to an object.
    */
    var responseObject ={};

    console.log(arguments);

    for(var i in arguments){
        console.log(arguments[i]);
        if( typeof( arguments[i] )===null || typeof( arguments[i] )===undefined ){
            continue;
        }else if( typeof( arguments[i] )=='number' ){

            if( responseObject.numbers==undefined ){
                responseObject.numbers = 0;
            }
            responseObject.numbers = responseObject.numbers + parseInt( arguments[i] );

        }else if( typeof( arguments[i] )=='string' ){

            if( responseObject.strings==undefined ){
                responseObject.strings = '';
            }
            responseObject.strings = responseObject.strings + arguments[i];

        }else if( typeof( arguments[i] )=='boolean' ){
            if( responseObject.boolean==undefined ){
                responseObject.boolean = {
                    true:0,
                    false:0
                }
            }
            if( arguments[i]===true ){
                responseObject.boolean.true++;
            }else{
                responseObject.boolean.false++;
            }
        }
    }
    console.log( responseObject );
    return responseObject;
};
 
module.exports = fnArgsChecker;