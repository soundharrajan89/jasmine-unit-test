describe("formatdate", function() {
    var formatdate = require('../src/formatdate');
    var checkformatdate;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkformatdate = new formatdate();
    });
 
    describe("formatdate function", function(){
         
        //Spec for sum operation
        it("returns the date in the follwing format: Day, date(suffix) Short-Month, Full year", function() {
            var sourceObject = checkformatdate.formatdateFunction('11-05-1988');
            console.log(sourceObject);
            expect(sourceObject).toEqual("Saturday, 5th Nov, 1988");
        });
         
    });
});