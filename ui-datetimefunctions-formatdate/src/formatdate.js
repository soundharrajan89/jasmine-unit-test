formatdate = function() {};
 
formatdate.prototype.formatdateFunction = function(dateValue){
    /*
        WAP to accept a date from the user. 
        User will enter the date in the format (dd-mm-yyyy). 
        Format the date in the following format: Tuesday, 15th Feb, 2017
    */

    console.log(dateValue);

    function date_addSuffix(i){
        var j = i % 10,
        k = i % 100;
        if (j == 1 && k != 11) {
            return i + "st";
        }
        if (j == 2 && k != 12) {
            return i + "nd";
        }
        if (j == 3 && k != 13) {
            return i + "rd";
        }
        return i + "th";
    }

    var response = (function dateCalculator(dateValue){
        var d = new Date(dateValue);
        var monthList = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var daylist = ['Monday', 'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        var monthShort = (monthList[d.getMonth()].toString()).substr(0,3);

        var output = {};
        output.date = date_addSuffix(d.getDate());
        output.month = monthShort;
        output.day = daylist[d.getDay()-1];
        output.year = d.getFullYear();
        return output.day + ', ' + output.date + ' ' + output.month + ', ' + output.year;
    })(dateValue);

    return response;
}

module.exports = formatdate;