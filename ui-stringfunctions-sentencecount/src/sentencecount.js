sentencecount = function() {};

sentencecount.prototype.sentencecountFunction = function(aVeryLongString){
    /*
        WAP that accepts a string from the user and counts the number of sentences.
    */
    var wordcount = 0;
    var words;
    var smallerwords = 0;
    console.log();
    var sentenceSplit = aVeryLongString.split( /[\.!?]+[ \n]+/g ); //splits text to sentences based on punctuation marks
    
    for( var s in sentenceSplit){
        var strippedSentence = sentenceSplit[s].replace(/[.,\/#!$%\^&\*;:'"{}=\-_`~()]/g,""); //remove all punctuation marks.
        strippedSentence = strippedSentence.trim(); //remove starting and ending spaces.
        console.log(strippedSentence);
        if(strippedSentence.replace(/\s+/g,"").length>0){
            //check if string contains characters after all spaces have been removed.
            wordcount++;
        }else{
            //no characters. String is empty
            sentenceSplit.splice(s,1);
        }
        
    }
    console.log(sentenceSplit);
    return wordcount;
}
 
module.exports = sentencecount;