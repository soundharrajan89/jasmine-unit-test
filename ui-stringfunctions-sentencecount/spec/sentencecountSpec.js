describe("sentencecount", function() {
    var sentencecount = require('../src/sentencecount');
    var checksentencecount;
 
    //This will be called before running each spec
    beforeEach(function() {
        checksentencecount = new sentencecount();
    });
 
    describe("sentencecount function", function(){
         
        //Spec for sum operation
        it("returns count of sentences in a text", function() {
            var string = "The Quick Brown Fox Jumps over the lazy dog.\n The Lazy dog screamed: \"Stop jumping over me!\" He then kicked the fox on his face."
            var response = 2;
            var sourceObject = checksentencecount.sentencecountFunction(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });
        
        it("returns count of sentences in a text", function() {
            var string = "The Quick Brown Fox   Jumps over the lazy dog. . .\n The Lazy dog screamed: \"Stop jumping over me!\" He then kicked the fox on his face."
            var response = 2;
            var sourceObject = checksentencecount.sentencecountFunction(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });

        it("returns count of sentences in a text", function() {
            var string = "Which is your favourite TV show? F.R.I.E.N.D.S is my favourite show";
            var response = 2;
            var sourceObject = checksentencecount.sentencecountFunction(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(response);
        });

    });
});