agecalculator = function() {};
 
agecalculator.prototype.agecalculatorFunction = function(userDate, checkDate){
    /*
        WAP that accepts birth date from the user and calculates the days between the current date and the user’s date. 
        Final output should contain number of years, months and days between the two dates.
    */
    var duser = new Date(userDate);
    var dcheck = new Date(checkDate);
    var dage = new Date(dcheck - duser);
    var epoch = new Date('1970-01-01');
    var outputAge = new Object();
    outputAge.years = dage.getFullYear() - epoch.getFullYear();
    outputAge.months = dage.getMonth() - epoch.getMonth();
    outputAge.days = dage.getDate() - epoch.getDate() - 1;

    return outputAge.years + ' years, ' + outputAge.months + ' months, ' + outputAge.days + ' days';    
}
 
module.exports = agecalculator;