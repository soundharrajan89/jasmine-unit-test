describe("agecalculator", function() {
    var agecalculator = require('../src/agecalculator');
    var checkagecalculator;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkagecalculator = new agecalculator();
    });
 
    describe("agecalculator function", function(){
         
        //Spec for sum operation
        it("returns age of the user as on 20-02-2018", function() {
            var sourceObject = checkagecalculator.agecalculatorFunction('11-05-1988','2-21-2018');
            console.log(sourceObject);
            expect(sourceObject).toEqual("29 years, 3 months, 17 days");
        });
         
    });
});