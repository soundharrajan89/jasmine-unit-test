resize = function() {};

resize.prototype.convert = function( arrayList, maxwidth ){
    /*
        WAP to accept an array and a max width.
        Based on the maximum value available in the array, assign proportional values to the other elements.
        Eg: If max width is 1000 and max value in array is 10, 10=>1000, 3=>300
        Return array with new values
    */

    var resizedList = [];
    var maxVal = Math.max(...arrayList); //ES6
    

    for(var i in arrayList){
        var divWidth = (arrayList[i]/maxVal)*maxwidth;
        resizedList.push( divWidth );
    }

    return resizedList;
};

resize.prototype.rearrange = function( arrayList, maxwidth ){
    /*
        WAP to accept an array and a max width.
        rearrange by making groups to match the largest number available in the array.
        For eg: If max value is 10, a group of 2,3,5 is the best combination to match the same.
        A value of 4,7 is invalid as it exceeds 10 but lower values are allowed (1,3,5) IF there are no more values available to map
    */

    function removeMaxes(number){
        if( number==maxWidth ){
            reArrangeList.push(number);
            reArrangeList.push(true);
        }
        return number< maxWidth;
    }

    var reArrangeList = []; //final list

    var calculatedWidths = this.convert( arrayList, maxwidth );
    var maxWidth = Math.max(...calculatedWidths);
    var minWidth = Math.min(...calculatedWidths);

    var markedList = calculatedWidths.filter(removeMaxes);
    debugger;

    while( markedList.length>0 ){

        //pick out the first element, map the remaining array against it to reach the max count
        var maxVal = markedList.shift();
        reArrangeList.push(maxVal);
        var counter = 0;
        while(maxVal<maxWidth && markedList[counter]!==undefined){
            if( (maxVal+markedList[counter])<=maxWidth ){ //maxval + next array element is less than or equal to max width
                maxVal = maxVal + markedList[counter]; //add to value
                reArrangeList.push(markedList[counter]); //add element to rearranged list
                markedList.splice( counter, 1 ); //remove the value from the array
            }else{
                counter++; //increment counter only if sum has not matched
            }
        }
        reArrangeList.push(true);
    }

    console.log( reArrangeList );

    return reArrangeList;
}

module.exports = resize;