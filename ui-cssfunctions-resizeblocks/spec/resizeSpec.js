describe("resize", function() {
    'use strict';
    var resize = require('../src/resize');
    var checkresize;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkresize = new resize();
    });
 
    describe("resize function", function () {

        //Spec for sum operation
        it("Creates divs by taking in the largest value from the array and the max width provided and formulating div sizes for other divs", function () {
            var arrayObj = [12,3,4,1,7,4,9,3,2,2,1,5,8,10,4,11];
            var divs = [1200,300,400,100,700,400,900,300,200,200,100,500,800,1000,400,1100];
            var response = checkresize.convert(arrayObj,1200);
            console.log(response);
            expect(response).toEqual(divs);
        });

    });

    describe("realign function", function () {

        //Spec for sum operation
        it("Rearranges divs based on the largest group that can be created based on maxWidth and the maxDiv value", function () {
            var arrayObj = [12,3,4,1,7,4,9,3,2,2,1,5,8,10,4,11];
            var divs = [1200, true, 300, 400, 100, 400, true, 700, 300, 200, true, 900, 200, 100, true, 500, 400, true, 800, true, 1000, true, 1100, true];
            var response = checkresize.rearrange(arrayObj,1200);
            console.log(response);
            console.log(response.length);
            console.log(divs.length);
            expect(response).toEqual(divs);
        });

    });
});