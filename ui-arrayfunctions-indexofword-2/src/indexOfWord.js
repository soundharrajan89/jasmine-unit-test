indexOfWord = function() {};

indexOfWord.prototype.indexOfWordFunction = function(arrayOfWords,word){
    /*
        WAP that accepts a multidimensional array of words from the user and checks for a particular word, returning the indexes.
    */
    var objectPath =[];

    function navigateArray( arrayOfWords, word ){
        //debugger;
        for(var i in arrayOfWords){
            if(Array.isArray(arrayOfWords[i])){ //if element is also an array, traverse further down the array
                if( navigateArray(arrayOfWords[i], word)!==false ){
                    objectPath.unshift(parseInt(i));
                    return true;
                }
            }else if( arrayOfWords[i] == word ){
                objectPath.push(parseInt(i));
                return true;
            }else{
            }
        }
        return false;
    }

    var output = navigateArray(arrayOfWords, word);
    
    if( output ){
        return objectPath;
    }  
    else{
        return false;
    } 
};
 
module.exports = indexOfWord;