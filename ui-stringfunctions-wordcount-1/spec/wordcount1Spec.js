describe("wordcount1", function() {
    var wordcount1 = require('../src/wordcount1');
    var checkwordcount1;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkwordcount1 = new wordcount1();
    });
 
    describe("wordcount1 function", function(){
         
        //Spec for sum operation
        it("returns number of words", function() {
            var string = "The Quick Brown Fox   Jumps over the lazy dog. . .\n The Lazy dog screamed: \"Stop jumping over me!\" He then kicked the fox on his face."

            var sourceObject = checkwordcount1.wordcount1Function(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(25);
        });

        it("returns number of words", function() {
            var string = "The Quick Brown Fox Jumps over the lazy dog. The Lazy dog screamed: 'Stop jumping over me!' and then kicked the fox on his face."

            var sourceObject = checkwordcount1.wordcount1Function(string);
            console.log(sourceObject);
            expect(sourceObject).toEqual(25);
        });
         
    });
});