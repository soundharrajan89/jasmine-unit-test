wordcount1 = function() {};

wordcount1.prototype.wordcount1Function = function(aVeryLongString){
    /*
        WAP that accepts a string from the user and counts the number of words in the string.
        Any special characters such as punctuation marks, etc should not be counted along with the words
    */
    var wordcount = 0;
    
    var sentences = aVeryLongString.split( /[\.!?]+[ \n]+/g );

    for( var s in sentences){
        sentences[s] = sentences[s].trim().replace(/[.,\/#!$%\^&\*;:'"{}=\-_`~()]/g,"");
        var words = sentences[s].split( /\s+/g );
        for( var w in words){
            if( words[w].length==0 || words[w]==''){
                words.splice(w,1);
            }
        }
        wordcount = wordcount + words.length;
        console.log(words);
    }
    console.log(sentences);

    return wordcount;
}
 
module.exports = wordcount1;