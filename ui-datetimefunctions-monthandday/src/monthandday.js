monthandday = function() {};
 
monthandday.prototype.monthanddayFunction = function(dateValue){
    /*
        WAP to accept a date from the user. 
        User will enter the date in the format (dd-mm-yyyy). 
        Check which month the date falls in, and find out the day of the week that falls on that date. 
        Output as an object: {month: January, day: Saturday}
    */
    var response = (function(dateValue){
        var d = new Date(dateValue);
        var monthlist = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var daylist = ['Monday', 'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
        var output = {
            month:String,
            day:String
        };
        output.month = monthlist[d.getMonth()];
        output.day = daylist[d.getDay()-1];
        return output;
    })(dateValue);

    return response;
}
 
module.exports = monthandday;