describe("monthandday", function() {
    var monthandday = require('../src/monthandday');
    var checkmonthandday;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkmonthandday = new monthandday();
    });
 
    describe("monthandday function", function(){
         
        //Spec for sum operation
        it("monthandday(date value) testing", function() {
            var responseDateObject = {
            	month: "February",
            	day: "Thursday"
            }
            var sourceObject = checkmonthandday.monthanddayFunction('2-04-1988');
            console.log(sourceObject);
            expect(sourceObject).toEqual(responseDateObject);
        });
         
    });
});