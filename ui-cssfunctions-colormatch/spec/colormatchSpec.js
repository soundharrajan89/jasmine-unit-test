describe("colormatch", function() {
    'use strict';
    var colormatch = require('../src/colormatch');
    var checkcolormatch;
 
    //This will be called before running each spec
    beforeEach(function() {
        checkcolormatch = new colormatch();
    });
 
    describe("colormatch function", function () {

        //Spec for sum operation
        it("returns HEX value of RGB color", function () {
            var response = checkcolormatch.toHex('255,110,20');
            console.log(response);
            expect(response).toEqual('#ff6e14');
        });

        //Spec for sum operation
        it("returns RGB value of HEX color", function () {
            var response = checkcolormatch.toRGB('#f47890');
            console.log(response);
            expect(response).toEqual([ 15, 7, 9 ]);
        });

    });
});