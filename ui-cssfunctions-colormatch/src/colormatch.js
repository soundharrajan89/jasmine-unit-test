colormatch = function() {};

colormatch.prototype.toHex = function( color ){
    /*
        WAP to convert an RGB color value to HEX
    */

    function componentToHex(c) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }
    
    function rgbToHex(r, g, b) {
        return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
    }
    

    //create RGB array
    var rgb = color.split(",");

    return rgbToHex( parseInt(rgb[0]), parseInt(rgb[1]), parseInt(rgb[2]) );

};

colormatch.prototype.toRGB = function( color ){
    /*
        WAP to convert an HEX color value to RGB
    */

    var rgb = [];

    if( color.indexOf('#')>-1 ){
        color = color.replace('#','');
        
        rgb.push( parseInt(color.slice(0,1),16) );
        rgb.push( parseInt(color.slice(2,3),16) );
        rgb.push( parseInt(color.slice(4,5),16) );
    }

    return rgb;
};
 
module.exports = colormatch;